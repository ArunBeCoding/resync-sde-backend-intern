# Back-end App Test

Back-end application development task. Back-end app should be developed in express.js framework. Database can be used SQL / NoSQL (Postgress, MySQL, MongoDB, InfluxDB, etc). Coding should be done using "camelCase" standard. Each function & business logic should have proper commenting.

**NOTE:** 
- Don't push the code in the master branch. Fork the branch and then start developing the codebase.
- Don't add node_modules directory
- create "backend" directory for Back-End codebase
- create "database" directory for datatabase file
- Nice to have feature - Docker & docker-compose.yml file [ Build and generate artifacts ]

**Problem Statement:**
Design and develop a back-end application which should return an Restful API for employee management.

**API List:**
1. `/login` API that checks if the username && password is correct
2. `/getEmployee/:e_id` API that provides detail of given employee_id
3. `/getEmployees` API that provides list of all employees
